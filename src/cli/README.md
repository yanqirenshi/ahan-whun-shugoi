# SRC/CLI/

## Description

AWS Cli のコマンドを生成するコードです。

## Usage

`aws.cli.command:make-aws-cli-command` をコールする感じです。

## Packages

| Name                         | Nickname        |
|------------------------------|-----------------|
| ahan-whun-shugoi.cli.command | aws.cli.command |
| ahan-whun-shugoi.cli.config  | aws.cli.config  |
| ahan-whun-shugoi.cli.option  | aws.cli.option  |
