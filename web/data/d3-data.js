var _NODES = [
    {
        _id: 1,
        name: "node-1",
        description: "",
        x: 100,
        y: 100
    },
    {
        _id: 2,
        name: "node-2",
        description: "",
        x: 200,
        y: 200
    },
    {
        _id: 3,
        name: "node-3",
        description: "",
        x: 300,
        y: 300
    }
];

var _LINKS = [
    {
        _id: 1,
        "from-id" : 1,
        "to-id" : 2,
        "edge-type" : "type-1",
        "relation" : ""
    }
];
